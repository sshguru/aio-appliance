# Description

A collection of Ansible code which you can use to quickly configure a Linux VM into an AIO server (router, DHCP, DNS, NTP) for your VMware vSphere with Workload Management (Tanzu!) homelab. 

# Before using this playbook

Make sure you review and adjust all vars listed in the aio-appliance.yml

If using aio-dns role, make sure to edit the zone files in ./roles/aio-dns/templates/[fwdmain.db.j2, reversemain.db.j2, reversework.db.j2] and add A & PTR records you need

# Usage

1. Download all the files in repo.
2. Install Ansible on your client machine
3. Edit the ansible.cfg and inventory files and adjust to your needs
4. Deploy a Linux VM running RHEL/CentOS/Fedora, assign it 2 NICs. Make sure it is reachable over the network from your client machine
5. Once deployed, add your SSH key to the system
5.1. ssh-keygen
5.2. ssh-copy-id user@linuxbox
6. Run the playbook with: ansible-playbook aio-appliance.yml
